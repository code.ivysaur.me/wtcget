# wtcget

![](https://img.shields.io/badge/written%20in-PHP-blue)

A scraper for the site webtoons.com that merges split images.

Tags: scraper


## Download

- [⬇️ wtcget.zip](dist-archive/wtcget.zip) *(1.30 KiB)*
